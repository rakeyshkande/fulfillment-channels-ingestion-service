package com.ftd.services.fulfillmentchannelsingestionservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FulfillmentChannelsIngestionServiceApplication.class)
public class FulfillmentChannelsIngestionServiceApplicationTests {

    @Test
    public void contextLoads() {
        FulfillmentChannelsIngestionServiceApplication.main(new String[]{});
    }
}
