package com.ftd.services.fulfillmentchannelsingestionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FulfillmentChannelsIngestionServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FulfillmentChannelsIngestionServiceApplication.class, args);
    }
}
