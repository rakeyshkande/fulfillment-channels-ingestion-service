#!/bin/bash
#
# docker-entrypoint for service

set -e

echo "Executing java ${JAVA_ARGS} "$@""
java ${JAVA_ARGS} -jar -Xms512m -Xmx2048m fulfillment-channels-ingestion-service-0.0.1-SNAPSHOT.jar
